DOSSEG 
 .MODEL SMALL 
 .STACK 0h 
 .DATA 
  a  dw 
  c  dw 
 .CODE 
; program blah;   
; var a,c: integer;   
; begin   
;  c:=1;   
MOV c,1 
;  a:=c+5;   
MOV ax,c 
ADD ax,5 
STORE M[1],ax 
MOV a,M[1] 
;  a:=10-5*-123;   
MOV ax,123 
NEG 
STORE M[2],ax 
MOV ax,5 
MUL ax,M[2] 
STORE M[3],ax 
MOV ax,10 
SUB ax,M[3] 
STORE M[4],ax 
MOV a,M[4] 
; end.   
MOV ax,4c00h 
INT 21h 
END 
