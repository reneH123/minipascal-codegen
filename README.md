Rene Haberland, 2005 Dresden  (haberland1@mail.ru)

This is a fully-handwritten lexer and parser for a Pascal-subset, called MiniPascal.  MiniPascal was developed as a branch of the Pascal0 repository.

Both syntax analyzers are written in Pascal (as MS DOS-application), with a code generation part based on the Inverse Polish Notation as intermediate representation.

The whole software was written by me, and is licensed under Creative Commons SA-NonCommercial version 3.
